package br.com.orionx.authentication.controller;

import org.nextframework.authorization.User;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MessageType;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.menu.MenuTag;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.authorization.bean.Usuario;
import br.com.orionx.util.autorizacao.dao.NextAuthorizationDAO;



/**
 * Controller que far� o login do usu�rio na aplica��o.
 * Se o login for efetuado com sucesso ir� redirecionar para /secured/Index
 */
@Controller(path="/autenticacao/login")
public class LoginController extends MultiActionController {
private static final String AFTER_LOGIN_GO_TO = "/";
	
	NextAuthorizationDAO authorizationDAO;
	
	public void setAuthorizationDAO(NextAuthorizationDAO authorizationDAO) {
		this.authorizationDAO = authorizationDAO;
	}
	
	/**
	 * Action que envia para a p�gina de login
	 */
	@DefaultAction
	public ModelAndView doPage(WebRequestContext request, Usuario usuario){
		return new ModelAndView("login", "usuario", usuario);
	}
	
	/**
	 * Efetua o login do usu�rio
	 */
	public ModelAndView doLogin(WebRequestContext request, Usuario usuario){
		String login = usuario.getLogin();
		//se foi passado o login na requisi��o, iremos verificar se o usu�rio existe e a senha est� correta
		if(login != null){
			//buscamos o usu�rio do banco pelo login
			User userByLogin = authorizationDAO.findUserByLogin(login);
			
			// se o usu�rio existe e a senha est� correta
			if(userByLogin != null && userByLogin.getPassword().equals(usuario.getPassword())){
				//Setando o atributo de se��o USER fazemos o login do usu�rio no sistema. 
				request.setUserAttribute("USER", userByLogin);
				
				//Limpamos o cache de permiss�es o menu.
				//O menu ser� refeito levando em considera��o as permiss�es do usu�rio
				request.setUserAttribute(MenuTag.MENU_CACHE_MAP, null);
				return new ModelAndView("redirect:"+AFTER_LOGIN_GO_TO);				
			}
			
			//Se o login e/ou a senha n�o estiverem corretos, avisar o usu�rio
			request.addMessage("Login e/ou senha inv�lidos", MessageType.ERROR);
		}
		
		//limpar o campo senha, e enviar para a tela de login j� que o processo falhou
		usuario.setSenha(null);
		return doPage(request, usuario);
	}

}