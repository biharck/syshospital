package br.com.orionx.authentication.filter;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.validation.annotation.MaxLength;

import br.com.orionx.authorization.bean.Uf;


public class MunicipioFiltro extends FiltroListagem{
	protected String nome;
	protected Uf uf;
	
	@DisplayName("Nome")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	public Uf getUf() {
		return uf;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
}
