package br.com.orionx.authorization.controller;

public class CotacoesExceptions extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CotacoesExceptions() {
		super();
	}

	public CotacoesExceptions(String message, Throwable cause) {
		super(message, cause);
	}

	public CotacoesExceptions(String message) {
		super(message);
	}

	public CotacoesExceptions(Throwable cause) {
		super(cause);
	}




}
