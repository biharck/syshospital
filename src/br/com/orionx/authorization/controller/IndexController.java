package br.com.orionx.authorization.controller;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

@Controller(path={"/autorizacao","/sistema/index","/assistencial/index","/financeiro/index","/apoio/index","/hotelaria/index"},
		authorizationModule=ProcessAuthorizationModule.class)
public class IndexController extends MultiActionController {
	@DefaultAction
	public ModelAndView action(WebRequestContext request){
		request.setAttribute("modulo",request.getFirstRequestUrl());
		return new ModelAndView("index");
	}
}
