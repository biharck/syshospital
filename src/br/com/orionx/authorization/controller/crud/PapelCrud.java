package br.com.orionx.authorization.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.orionx.authorization.bean.Papel;


@Controller(path = "/autorizacao/crud/papel", authorizationModule = CrudAuthorizationModule.class)
public class PapelCrud extends br.com.orionx.authorization.controller.CrudController<FiltroListagem, Papel, Papel> {

}
