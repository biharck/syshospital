package br.com.orionx.authorization.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.orionx.authorization.bean.Cliente;
import br.com.orionx.authorization.controller.CrudController;

@Controller(path="/hotelaria/crud/Cliente",authorizationModule=CrudAuthorizationModule.class)
public class ClienteCrud extends CrudController<FiltroListagem, Cliente, Cliente>{

}
