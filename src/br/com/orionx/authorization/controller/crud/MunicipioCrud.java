package br.com.orionx.authorization.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.bean.annotation.CrudBean;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.orionx.authentication.filter.MunicipioFiltro;
import br.com.orionx.authorization.bean.Municipio;
import br.com.orionx.util.SysHospitalarUtil;

@CrudBean
@Controller(path="/autorizacao/crud/Municipio", authorizationModule=CrudAuthorizationModule.class)
public class MunicipioCrud extends CrudController<MunicipioFiltro, Municipio, Municipio> {
	
	@Override
	protected void salvar(WebRequestContext request, Municipio bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
//			if (DatabaseError.isKeyPresent(e, "IDX_MUNICIPIO_NOME")) throw new SysHospitalarE("Cidade j� cadastrada no sistema.");
			
		}
	}	
	
	@Override
	protected void listagem(WebRequestContext request, MunicipioFiltro filtro)throws Exception {
		super.listagem(request, filtro);
	}
}