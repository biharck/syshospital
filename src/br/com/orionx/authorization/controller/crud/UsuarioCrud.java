package br.com.orionx.authorization.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;

import br.com.orionx.authorization.bean.Usuario;
import br.com.orionx.util.autorizacao.service.PapelService;






@Controller(path="/autorizacao/crud/usuario",authorizationModule=CrudAuthorizationModule.class)
public class UsuarioCrud extends br.com.orionx.authorization.controller.CrudController<FiltroListagem,Usuario,Usuario> {
	
	private PapelService papelService;
	
	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}

	@Override
	protected void entrada(WebRequestContext request, Usuario form) throws Exception {
		request.setAttribute("listaPapel", papelService.findAll());
	}
	
}
