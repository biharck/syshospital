package br.com.orionx.authorization.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudController;

import br.com.orionx.authorization.bean.Uf;

@Controller(path="/autorizacao/crud/Uf", authorizationModule=CrudAuthorizationModule.class)
public class UfCrud extends CrudController<org.nextframework.controller.crud.FiltroListagem, Uf, Uf>{

}
