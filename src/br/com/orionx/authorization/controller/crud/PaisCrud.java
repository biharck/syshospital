package br.com.orionx.authorization.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.orionx.authorization.bean.Pais;
import br.com.orionx.authorization.controller.CrudController;

@Controller(path="/sistema/crud/Pais",authorizationModule=CrudAuthorizationModule.class)
public class PaisCrud extends CrudController<FiltroListagem, Pais, Pais>{

}
