package br.com.orionx.authorization.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.authorization.bean.Empresa;
import br.com.orionx.authorization.controller.CrudController;


@Controller(path="/sistema/crud/Empresa",authorizationModule=CrudAuthorizationModule.class)
public class EmpresaCrud extends CrudController<FiltroListagem, Empresa, Empresa>{
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Empresa form)
			throws CrudException {
		request.setAttribute("setIdForm", "form");
		return super.doEntrada(request, form);
	}

}
