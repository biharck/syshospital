package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ramal")
@SequenceGenerator(name="sq_ramal", sequenceName="sq_ramal")
public class Ramal {
	
	private Integer idRamal;
	private String numero;
	private String localizacao;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ramal")
	public Integer getIdRamal() {
		return idRamal;
	}
	public String getNumero() {
		return numero;
	}
	public String getLocalizacao() {
		return localizacao;
	}
	public void setIdRamal(Integer idRamal) {
		this.idRamal = idRamal;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}
	

}
