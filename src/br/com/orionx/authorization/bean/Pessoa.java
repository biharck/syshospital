package br.com.orionx.authorization.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;



@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name = "sq_pessoa", sequenceName = "sq_pessoa")
public class Pessoa {

	protected Long idPessoa;
	protected String email;
	protected String nome;
	protected String cpf;	
	protected String observacao;
	protected String telefone;
	protected String celular;
	protected String endereco;
	protected String bairro;
	protected String cep;
	protected Uf uf;
	protected Municipio municipio;
	protected Date dtNascimento;
//	protected EstadoCivil estadoCivil;
//	protected Escolaridade escolaridade;
	protected String naturalidade;
	protected String rg;
	protected String emissaoRg;
	protected String orgaoExp;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pessoa")
	public Long getIdPessoa() {
		return idPessoa;
	}
	public String getEmail() {
		return email;
	}
	public String getNome() {
		return nome;
	}
	public String getCpf() {
		return cpf;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCep() {
		return cep;
	}
	@Transient
	public Uf getUf() {
		return uf;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idMunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public Date getDtNascimento() {
		return dtNascimento;
	}
	public String getNaturalidade() {
		return naturalidade;
	}
	public String getRg() {
		return rg;
	}
	public String getEmissaoRg() {
		return emissaoRg;
	}
	public String getOrgaoExp() {
		return orgaoExp;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}
	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public void setEmissaoRg(String emissaoRg) {
		this.emissaoRg = emissaoRg;
	}
	public void setOrgaoExp(String orgaoExp) {
		this.orgaoExp = orgaoExp;
	}

}
