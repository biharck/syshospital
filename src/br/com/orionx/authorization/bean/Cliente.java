package br.com.orionx.authorization.bean;

import javax.persistence.Entity;

@Entity
public class Cliente extends Pessoa{
	
	private String matricula;
	private String nomeResponsavel;
	private String telResponsavel;
	private String telResponsavel2;
	
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getNomeResponsavel() {
		return nomeResponsavel;
	}
	public String getTelResponsavel() {
		return telResponsavel;
	}
	public String getTelResponsavel2() {
		return telResponsavel2;
	}
	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}
	public void setTelResponsavel(String telResponsavel) {
		this.telResponsavel = telResponsavel;
	}
	public void setTelResponsavel2(String telResponsavel2) {
		this.telResponsavel2 = telResponsavel2;
	}

}
