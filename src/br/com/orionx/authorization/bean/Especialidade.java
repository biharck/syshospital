package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="especialidade")
@SequenceGenerator(name="sq_especialidade",sequenceName="sq_especialidade")
public class Especialidade {
	
	private Integer idEspecialidade;
	private String nome;
	private String descricao;
	private Papel papel;
	
	@Id
	@GeneratedValue(generator="sq_especialidade",strategy=GenerationType.AUTO)
	public Integer getIdEspecialidade() {
		return idEspecialidade;
	}
	@Required
	@MaxLength(value = 100)
	public String getNome() {
		return nome;
	}
	@Required
	@MaxLength(value = 200)
	public String getDescricao() {
		return descricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PAPEL")
	public Papel getPapel() {
		return papel;
	}
	public void setIdEspecialidade(Integer idEspecialidade) {
		this.idEspecialidade = idEspecialidade;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	

}
