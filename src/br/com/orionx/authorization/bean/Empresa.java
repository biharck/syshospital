package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

/**
 * Representa Hospital/Clinica....
 * @author Biharck
 *
 */
@Entity
@Table(name="empresa")
@SequenceGenerator(name="sq_empresa", sequenceName="sq_empresa")
public class Empresa {
	
	private Integer idEmpresa;
	private String razaoSocial;
	private String cnpj;
	private Integer cnes;
	private String endereco;
	private String bairro;
	private String cep;
	private Municipio municipio;
	private Uf uf;
	private String telefoneGeral;
	private Ramal ramal;
	private String fax;
	private String email;
	private String confirmaEmail;
	private String incricaoEstadual;
	private String inscricaoMunicipal;
	
	/**
	 * Transient
	 * @return
	 */
	@Transient
	
	public String getConfirmaEmail() {
		return confirmaEmail;
	}
	public void setConfirmaEmail(String confirmaEmail) {
		this.confirmaEmail = confirmaEmail;
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	@MaxLength(value=200)
	@Required
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@Required
	public String getCnpj() {
		return cnpj;
	}
	@MaxLength(value=50)
	@Required
	public Integer getCnes() {
		return cnes;
	}
	@MaxLength(value=200)
	@Required
	public String getEndereco() {
		return endereco;
	}
	@MaxLength(value=100)
	@Required
	public String getBairro() {
		return bairro;
	}
	@MaxLength(value=20)
	@Required
	public String getCep() {
		return cep;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idMunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idUf")
	public Uf getUf() {
		return uf;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idRamal")
	public Ramal getRamal() {
		return ramal;
	}
	public String getTelefoneGeral() {
		return telefoneGeral;
	}
	public String getFax() {
		return fax;
	}
	public String getEmail() {
		return email;
	}
	public String getIncricaoEstadual() {
		return incricaoEstadual;
	}
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCnes(Integer cnes) {
		this.cnes = cnes;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setTelefoneGeral(String telefoneGeral) {
		this.telefoneGeral = telefoneGeral;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setIncricaoEstadual(String incricaoEstadual) {
		this.incricaoEstadual = incricaoEstadual;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public void setRamal(Ramal ramal) {
		this.ramal = ramal;
	}
}
