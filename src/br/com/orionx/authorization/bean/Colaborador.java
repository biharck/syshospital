package br.com.orionx.authorization.bean;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class Colaborador extends Pessoa {
	private Usuario usuario;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="Id")
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public Long getIdPessoa() {
		// TODO Auto-generated method stub
		return super.getIdPessoa();
	}
	
	

}
