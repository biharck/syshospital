package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_tipo_convenio", sequenceName = "sq_tipo_convenio")
@Table(name="tipoconvenio")
public class TipoConvenio {
	
	private Integer idTipoConvenio;
	private String nome;
	
	@Id
	@GeneratedValue(generator="sq_tipo_convenio",strategy=GenerationType.AUTO)
	public Integer getIdTipoConvenio() {
		return idTipoConvenio;
	}
	@Required
	@MaxLength(value = 100)
	public String getNome() {
		return nome;
	}
	public void setIdTipoConvenio(Integer idTipoConvenio) {
		this.idTipoConvenio = idTipoConvenio;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
