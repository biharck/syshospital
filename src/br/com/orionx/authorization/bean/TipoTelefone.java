package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="tipoTelefone")
@SequenceGenerator(name="sq_tipo_telefone", sequenceName="sq_tipo_telefone")
public class TipoTelefone {
	
	private Integer idTipoTelefone;
	private String descricao;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_tipo_telefone")
	public Integer getIdTipoTelefone() {
		return idTipoTelefone;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setIdTipoTelefone(Integer idTipoTelefone) {
		this.idTipoTelefone = idTipoTelefone;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
