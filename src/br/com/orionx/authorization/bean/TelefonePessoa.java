package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="telefonePessoa")
@SequenceGenerator(name="sq_telefone_pessoa", sequenceName="sq_telefone_pessoa")
public class TelefonePessoa {
	
	private Integer idTelefonePessoa;
	private String ddd;
	private String numero;
	private TipoTelefone tipoTelefone;
	private Pessoa pessoa;
	
	@Id
	@GeneratedValue(generator="sq_telefone_pessoa",strategy=GenerationType.AUTO)
	public Integer getIdTelefonePessoa() {
		return idTelefonePessoa;
	}
	public String getDdd() {
		return ddd;
	}
	public String getNumero() {
		return numero;
	}
	@JoinColumn(name="idTipoTelefone")
	@ManyToOne(fetch=FetchType.LAZY)
	public TipoTelefone getTipoTelefone() {
		return tipoTelefone;
	}
	
	@JoinColumn(name="idPessoa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setIdTelefonePessoa(Integer idTelefonePessoa) {
		this.idTelefonePessoa = idTelefonePessoa;
	}
	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setTipoTelefone(TipoTelefone tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	
	

}
