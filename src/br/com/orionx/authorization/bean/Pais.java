package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="pais")
@SequenceGenerator(name="sq_pais",sequenceName="sq_pais")
public class Pais {
	
	private Integer idPais;
	private String nome;
	private String sigla;
	
	@Id
	@GeneratedValue(generator="sq_pais",strategy=GenerationType.AUTO)
	public Integer getIdPais() {
		return idPais;
	}
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	public String getSigla() {
		return sigla;
	}
	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	

}
