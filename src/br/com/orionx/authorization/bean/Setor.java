package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_setor", sequenceName = "sq_setor")
@Table(name="setor")
public class Setor {
	
	private Integer idSetor;
	private String nome;
	
	@Id
	@GeneratedValue(generator="sq_setor",strategy=GenerationType.AUTO)
	public Integer getIdSetor() {
		return idSetor;
	}
	@Required
	@MaxLength(value=100)
	public String getNome() {
		return nome;
	}
	public void setIdSetor(Integer idSetor) {
		this.idSetor = idSetor;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}
