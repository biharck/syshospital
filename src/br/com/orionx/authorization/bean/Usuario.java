package br.com.orionx.authorization.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.authorization.User;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Password;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

/**
 * Representa um usu�rio no sistema
 */
@Entity
@Table(name="usuario")
@SequenceGenerator(name="SQ_USUARIO", sequenceName="SQ_USUARIO")
public class Usuario implements User {
    
	private Long id;
	private String nome;
	private String login;
	private String senha;
	
	private List<Papel> papeis;
    
	@Transient
	public String getPassword() {
		return getSenha();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SQ_USUARIO")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@DescriptionProperty
    @Required
    @MaxLength(value=50)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Required
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Password
    @Required
    @MaxLength(value=20)
    @MinLength(value=4)
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Transient
	public List<Papel> getPapeis() {
		return papeis;
	}

	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}


}