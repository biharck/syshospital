package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="ramaisempresa")
@SequenceGenerator(name="sq_ramais_empresa",sequenceName="sq_ramais_empresa")
public class RamaisEmpresa {
	
	private Integer idRamaisEmpresa;
	private String numero;
	private String localizacao;
	private Empresa empresa;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_ramais_empresa")
	public Integer getIdRamaisEmpresa() {
		return idRamaisEmpresa;
	}
	@MaxLength(value=8)
	@Required
	public String getNumero() {
		return numero;
	}
	@MaxLength(value=100)
	@Required
	public String getLocalizacao() {
		return localizacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEmpresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setIdRamaisEmpresa(Integer idRamaisEmpresa) {
		this.idRamaisEmpresa = idRamaisEmpresa;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	

}
