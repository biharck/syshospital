package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "SQ_SEXO", sequenceName = "SQ_SEXO")
@Table(name="sexo")
public class Sexo {

	protected Integer cdsexo;
	protected String nome;
	
	public static final Integer MASCULINO = 1;
	public static final Integer FEMININO = 2;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SQ_SEXO")
	public Integer getCdsexo() {
		return cdsexo;
	}
	public void setCdsexo(Integer id) {
		this.cdsexo = id;
	}

	
	@Required
	@MaxLength(10)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
