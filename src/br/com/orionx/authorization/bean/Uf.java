package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "SQ_UF", sequenceName = "SQ_UF")
@Table(name="uf")
public class Uf {

	protected Integer idUf;
	protected String nome;
	protected String sigla;
	private Pais pais;
	
	public static Uf MINAS_GERAIS = new Uf(13);
	
	public Uf(){}
	public Uf(Integer idUf){
		this.idUf = idUf;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SQ_UF")
	public Integer getIdUf() {
		return idUf;
	}
	public void setIdUf(Integer idUf) {
		this.idUf = idUf;
	}

	
	@Required
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@Required
	@MaxLength(2)
	@DescriptionProperty
	public String getSigla() {
		return sigla;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idPais")
	public Pais getPais() {
		return pais;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
}
