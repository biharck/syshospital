package br.com.orionx.authorization.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_leito", sequenceName = "sq_leito")
@Table(name="leito")
public class Leito {
	
	private Integer idLeito;
	private String nome;
	private Setor setor;
	
	@Id
	@GeneratedValue(generator="sq_leito",strategy=GenerationType.AUTO)
	public Integer getIdLeito() {
		return idLeito;
	}
	@MaxLength(value=100)
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idSetor")
	public Setor getSetor() {
		return setor;
	}
	public void setIdLeito(Integer idLeito) {
		this.idLeito = idLeito;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
	}
	
	

}
