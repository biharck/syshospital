package br.com.orionx.util.autorizacao.dao;

import org.nextframework.persistence.GenericDAO;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.orionx.authorization.bean.Papel;
import br.com.orionx.authorization.bean.PapelUsuario;
import br.com.orionx.authorization.bean.Usuario;




public class UsuarioDAO extends GenericDAO<Usuario> {
	
	PapelDAO papelDAO;
	PapelUsuarioDAO papelUsuarioDAO;
	
	public void setPapelUsuarioDAO(PapelUsuarioDAO papelUsuarioDAO) {
		this.papelUsuarioDAO = papelUsuarioDAO;
	}
	
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	
	public Usuario findByLogin(String login) {
		return query()
			.where("UPPER(usuario.login) = ?", login.toUpperCase())
			.unique();
	}

	@Override
	public Usuario loadForEntrada(Usuario bean) {
		bean = super.loadForEntrada(bean);
		bean.setPapeis(papelDAO.findByUsuario(bean));
		return bean;
	}
	

	@Override
	public void saveOrUpdate(final Usuario bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				Usuario usuarioSalvo = findByLogin(bean.getLogin());
				if(usuarioSalvo!=null){
					boolean ehEdicao = bean.getId()!=null;
					boolean ehMesmoObjeto = usuarioSalvo.getId().equals(bean.getId());
					if (!ehEdicao || ehEdicao && !ehMesmoObjeto){
						throw new RuntimeException("O login j� existe, digite outro login");
					}
				}
				
				UsuarioDAO.super.saveOrUpdate(bean);
				papelDAO.deleteByUsuario(bean);
				if(bean.getPapeis() != null){
					for (Papel papel : bean.getPapeis()) {
						PapelUsuario papelUsuario = new PapelUsuario();
						papelUsuario.setPapel(papel);
						papelUsuario.setUsuario(bean);
						papelUsuarioDAO.saveOrUpdate(papelUsuario);
					}
				}
				return null;
			}
			
		});
	}
}
