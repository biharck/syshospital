package br.com.orionx.util;

import org.nextframework.authorization.User;
import org.nextframework.core.standard.Next;

import br.com.orionx.authorization.bean.Usuario;



/**
 * <p>Classe coposta por m�todos que s�o �teis em todo o sistema
 * @author Biharck
 *
 */
public class SysHospitalarUtil {

	/**
	 * <p>M�todo que retorna o usu�rio logado no sistema
	 * @return {@link Usuario}
	 * @author Biharck
	 */
	public static Usuario getPessoaLogada() {
		User user = Next.getRequestContext().getUser();
		if (user instanceof Usuario) {
			Usuario pessoa = (Usuario) user;
			return pessoa;
		} else {
			return null;
		}
	}
}
