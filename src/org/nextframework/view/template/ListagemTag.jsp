<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="${listagemTag.titulo}" validateForm="false">
		<input type="hidden" name="notFirstTime" value="true"/>
		<div class="linkBar">
			<c:if test="${listagemTag.showNewLink || !empty listagemTag.linkArea}">
				${listagemTag.invokeLinkArea}
				<c:if test="${listagemTag.showNewLink}">						
					<n:link action="criar">Novo</n:link>
				</c:if>						
			</c:if>	
		</div>
	
		<div>
			<n:doBody />
		</div>

</t:tela>
