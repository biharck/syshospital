<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>


<%@page import="org.nextframework.core.web.NextWeb"%><t:tela titulo="${entradaTag.titulo}" validateForm="false">
		<c:if test="${consultar}">
			<input type="hidden" name="forcarConsulta" value="true"/>
		</c:if>
		<c:if test="${param.fromInsertOne == 'true'}">
			<input type="hidden" name="fromInsertOne" value="true"/>
		</c:if>

		<div class="linkBar">
			<c:if test="${entradaTag.showListagemLink || !empty entradaTag.linkArea}">
				${entradaTag.invokeLinkArea}			
				<c:if test="${entradaTag.showListagemLink}">
						<button class="modalInputvoltar buttonaction" rel="#voltar">Voltar</button>
				</c:if>				
			</c:if>	
		</div>

		<div>
			<n:bean name="${TEMPLATE_beanName}">
			<n:doBody />
			</n:bean>
		</div>
</t:tela>
<!-- user input dialog -->
<!-- yes/no dialog -->
<div class="modal" id="voltar">
	<h2>Voltar a Listagem</h2>

	<p>
		Deseja realmente voltar a listagem sem salvar os dados?
	</p>

	<!-- yes/no buttons -->
	<p>
		<button class="close"> Sim </button>
		<button class="close"> N�o </button>

	</p>
</div>





<script>
// What is $(document).ready ? See: http://flowplayer.org/tools/using.html#document_ready
$(document).ready(function() {

	var triggers = $("button.modalInputvoltar").overlay({
	
		// some expose tweaks suitable for modal dialogs
		expose: {
			color: '#333',
			loadSpeed: 200,
			opacity: 0.9
		},
	
		closeOnClick: false
	});
	
	var buttons = $("#voltar button").click(function(e) {
		
		// get user input
		var yes = buttons.index(this) === 0;
		var url = "<%= NextWeb.getRequestContext().getServletRequest().getContextPath()+""+NextWeb.getRequestContext().getFirstRequestUrl()+"?ACAO=listagem" %>";
		
		if(yes)
			location.href=""+url+"";
	});

});

//execute your scripts when the DOM is ready. this is a good habit
$(function() {
	//select all desired input fields and attach tooltips to them
	$("#form :input").tooltip({
	
		// place tooltip on the right edge
		position: "center right",
	
		// a little tweaking of the position
		offset: [-2, 10],
	
		// use the built-in fadeIn/fadeOut effect
		effect: "fade",
	
		// custom opacity setting
		opacity: 0.7,
	
		// use this single tooltip element
		tip: '.tooltip'
	
	});
});
</script>




