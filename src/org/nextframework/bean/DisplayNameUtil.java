/*
 * Next Framework http://www.nextframework.org
 * Copyright (C) 2009 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package org.nextframework.bean;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.util.ReflectionCache;
import org.nextframework.util.ReflectionCacheFactory;
import org.nextframework.util.Util;

import br.com.orionx.annotation.TitleToolTip;

public class DisplayNameUtil {

	static Map<Class<?>, String> cacheClassDisplayName = new HashMap<Class<?>, String>();
	/**
	 * Retorna o displayName da classe utiliza cache
	 * @param clazz
	 * @return
	 */
	public static String getDisplayName(Class<?> clazz) {
		boolean reloadCache = reloadCache();
		if(reloadCache){ // nao � thread safe... o arquivo displayNames n�o deve ser modificado quando multiplas Threads estiverem sendo utilizadas
			cacheClassDisplayName = new HashMap<Class<?>,String>();
		}
		//String displayName = cacheClassDisplayName.get(clazz);
		String displayName = null;
		
		if (displayName == null) {
			ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
			if(reflectionCache.isAnnotationPresent(clazz, TitleToolTip.class))
				System.out.println(reflectionCache.getAnnotation(clazz,TitleToolTip.class).value());
			if (reflectionCache.isAnnotationPresent(clazz, DisplayName.class)) {
				displayName = reflectionCache.getAnnotation(clazz,DisplayName.class).value();
			}
			if(displayName == null){
				//I18N
				String[] names = {
						clazz.getName(),
						clazz.getSimpleName(),
						Util.strings.uncaptalize(clazz.getSimpleName())
				};
				for (int i = 0; i < names.length; i++) {
					displayName = Util.locale.getBundleKey(names[i]);	
					if(displayName != null)
						break;	
				}				
			}
			if(displayName == null){
				displayName = Util.strings.separateOnCase(clazz.getSimpleName());
			}
			
			cacheClassDisplayName.put(clazz, displayName);
		}
		return displayName;
	}
	

	static Map<Class<?>, Map<String, String>> cachePropertyDisplayName = new HashMap<Class<?>, Map<String, String>>();
	
	public static String getDisplayName(Class<?> clazz, String propertyName, Annotation[] annotations){
		boolean reloadCache = reloadCache();
		if(reloadCache){ // nao � thread safe... o arquivo displayNames n�o deve ser modificado quando multiplas Threads estiverem sendo utilizadas
			cachePropertyDisplayName = new HashMap<Class<?>, Map<String, String>>();
		}
		
		
		Map<String, String> map = cachePropertyDisplayName.get(clazz);
		if(map == null){
			map = new HashMap<String, String>();
			cachePropertyDisplayName.put(clazz, map);
		}
		
		
		//String displayName = map.get(propertyName);
		String displayName = null;
		
		if (displayName == null) {
			
			for (Annotation anno : annotations) {
				if (DisplayName.class.isAssignableFrom(anno.annotationType())) {
					displayName = ((DisplayName) anno).value();
				}
			}
			if(displayName == null){ // tentar do displayNames.properties
				//I18N
				String[] names = {
						clazz.getName()+"."+propertyName,
						clazz.getSimpleName()+"."+propertyName,
						Util.strings.uncaptalize(clazz.getSimpleName())+"."+propertyName,
						propertyName
				};
				for (int i = 0; i < names.length; i++) {
					displayName = Util.locale.getBundleKey(names[i]);
					if (displayName != null)
						break;
				}
			}
			if (displayName == null) {
				displayName = Util.strings.separateOnCase(Util.strings.captalize(propertyName));
			}
			
			map.put(propertyName, displayName);
		}
		return displayName;
	}

	static boolean neverReload = false;
	static long lastRead = 0;
	
	public static boolean reloadCache() {
		//TODO SEMPRE VAI LER O ARQUIVO PORQUE ClassLoader.getSystemResource("displayNames.properties");
		//N�O FUNCIONA EM J2EE... VAI SER TROCADO QUANDO ARRUMAR UMA FORMA DE VER A DATA DO ARQUIVO
		//o resourceBundle j� faz cache
		if(1==1){
			return true;
		}
		if(neverReload){
			return false;
		}
		//faz o reload do cache se necess�rio
		File file = null;
		boolean reloadCache = false;
		try {
			URL systemResource = ClassLoader.getSystemResource("displayNames.properties");
			if (systemResource != null) {
				file = new File(systemResource.toURI());
				if (file != null && file.exists()) {
					if (lastRead != file.lastModified()) {
						reloadCache = true;
						lastRead = file.lastModified();
					}
				}
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return reloadCache;
	}
	

}
