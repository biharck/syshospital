<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!--
Design by Orionx
http://www.orionx.com.br
-->
<%@page import="br.com.orionx.util.SysHospitalarUtil"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<n:head/>
	<script type="text/javascript" src="/SysHospital/js/jquery.maskedinput-1.1.1.js"></script>
	<script type="text/javascript" src="/SysHospital/js/crir.js"></script>
	<link href="SysHospital/css/crir.css" rel="stylesheet" type="text/css" />
	<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
	<title>Sys Hospital</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />

	<!--[if IE]>
	<style type="text/css">
	#sidebar #calendar {
		background-position: 0px 20px;
	}
	</style>
	<![endif]-->
	</head>
	
	<body>
		<script type="text/javascript" src="js/jquery.tools.min.js"></script>
		
		<div id="logo">	
			<h2><a href="#">English Version</a> | <a href="#">Portuguese Version</a> | <a href="#">Spanish Version</a></h2>
		</div>
		
		<div id="sub-header">
			<div id="openMenuBar" class="menu-sub-modulo menubar" onclick="javascript:openMenu(1)"></div>	
			<div id="openMenuBar" class="menu-sub-modulo menubar_left" onclick="javascript:openMenu(1)"></div>
		</div>
		
		<!-- Menu accordion dos modulos -->
		<div id="accordion">
			<img class="current" src="/SysHospital/images/up-module.png"/>
				<div style="width: 200px; display: block;">
					<h3>�lceras de Press�o</h3>
					<p> teste de informa��o </p>
				</div>
				
			<img class="" src="/SysHospital/images/med-care-module.png"/>
				<div>
					<h3>Med Care</h3>
					<p> teste de informa��o </p>
				</div>
	
			<img class="" src="/SysHospital/images/med-hospital-module.png"/>
				<div>
					<h3>Med Hospital</h3>
					<p> teste de informa��o </p>
				</div>
			
			<img class="" src="/SysHospital/images/med-hospital-module.png"/>
				<div>
					<h3>Med Hospital</h3>
					<p> teste de informa��o </p>
				</div>
		
		</div>
		<hr />
		
		<!-- alinhamentos da barra banner -->
		<div id="banner">
			<div class="menu-modulo-right">
			</div>
			
			<div class="menu-modulo-left-icon-module">
				<button type="image" class="modalInput buttonnew" href="#" rel="#prompt">
			</div>
			
			<div class="menu-modulo-left-colaborador photo-colaborador">
				
			</div>
			<div class="menu-modulo-left-nome">
				<strong><center><% out.print(SysHospitalarUtil.getPessoaLogada().getNome()); %></center></strong>
				<small><strong><center>Enfermeira Nefrologista</center></strong></small>			
			</div>
			<div class="menu-modulo-left">
				<center>
					<c:if test="${empty mostraMenu}">
						<c:if test="${modulo == '/sistema/index'}">
							<n:menu menupath="/WEB-INF/menu/sistema/sistema.xml"/>
						</c:if>
						<c:if test="${modulo == '/autorizacao'}">
							<n:menu menupath="/WEB-INF/menu.xml"/>
						</c:if>
						<c:if test="${modulo == '/hotelaria/index'}">
							<n:menu menupath="/WEB-INF/menu/hotelaria/recepcao.xml"/>
						</c:if>
						
					</c:if>
				</center>
			</div>
		
		</div>
		
		<!-- start page -->
		<div id="page">
			<!-- come�a sub-menu -->
						<!-- user input dialog --> 
			<div class="modal" id="prompt"> 
			    <style> 
			/* navigation */
			#nav {
				background:#ddd ;
				border-bottom:1px solid #CCCCCC;
				//height:156px;	
				height:308px;
				width:745px;
			}
			 
			#nav ul {	
				width:600px;
				margin:0 auto;	
			}
			 
			#nav li {	
				border-right:1px solid #ddd;
				float:left;
				padding-left:1px;
				width:175px;
				list-style-type:none;
				text-align:center;
			}
			 
			#nav a {
				color:#333333;
				display:block;
				padding:17px;
				position:relative;
				word-spacing:-2px;
				font-size:11px;		
				height:122px;	
				text-decoration:none;
			}	
			 
			#nav a.current {
				background:url(http://static.flowplayer.org/tools/img/tabs/down_large.jpg);	
			}
			 
			#nav img {
				background-color:none;
				border:none;
				margin:-17px 0 5px 27px;
				padding:4px;		
				display:block;
			}
			 
			#nav strong {
				display:block;		
				font-size:13px;
			}
			 
			/* panes */
			#panes {
				background:#fff  repeat scroll 0 0;
				border-color:#ccc;
				border-style:solid;
				border-width:1px 1px 0;
				width:743px;	
				height:255px;
				margin-bottom:-20px;
				padding-bottom:20px;
				
				/* must be relative so the individual panes can be absolutely positioned */
				position:relative;
			}
			 
			/* crossfading effect needs absolute positioning from the elements */
			#panes div {
				display:none;		
				position:absolute;
				top:20px;
				left:20px;
				font-size:14px;
				color:#444;	
				width:650px; 
			}
			 
			#panes img {
				float:left;
				margin-right:20px;		
			}
			 
			#panes p.more {
				color:#000;
				font-weight:bold;
			}
			 
			#panes h3 {
				margin:0 0 -5px 0;
				font-size:22px;
				font-weight:normal;
			}
			 
			.overlay {
				display:none;
				width:500px;
				padding:20px;
				background-color:#ddd;
			}
			</style> 
			 <!-- navigator --> 
			<div id="nav"> 
				<ul> 		
					<li> 
						<a href="#1"> 
							<img src="/SysHospital/images/modules/assistencial.png"/> 
							<strong>Gest�o Assistencial</strong> 
							
						</a> 
					</li> 
					<li> 
						<a href="#2"> 
							<img src="/SysHospital/images/modules/hotelaria.png"/> 
							<strong>Gest�o de Hotelaria</strong> 
							
						</a> 
					</li> 
					<li> 
						<a href="#3"> 
								<img src="/SysHospital/images/modules/apoio.png"/> 
							<strong>Gest�o de Apoio</strong> 
							
						</a> 
					</li> 
					<li> 
						<a href="#4"> 
							<img src="/SysHospital/images/modules/adm.png" /> 
							<strong>Gest�o Administrativa e Financeira</strong> 
							
						</a> 
					</li>
					<li> 
						<a href="#5"> 
							<img src="/SysHospital/images/modules/estrategica.png" /> 
							<strong>Gest�o Estrat�gica</strong> 
							
						</a> 
					</li> 
					<li> 
						<a href="#6"> 
							<img src="/SysHospital/images/modules/administrador.png" /> 
							<strong>Administra��o do Sistema</strong> 
							
						</a> 
					</li>
				</ul> 
			</div>
			<!-- tab panes --> 
			<div id="panes"> 
			 
				<div> 
					<h3>Gest�o Assistencial</h3> 
					<br><br>					
					<img src="/SysHospital/images/modules/assistencial/PlanoCuidados.png"  />
					<img src="/SysHospital/images/modules/assistencial/PlanoCuidadosMobile.png"  />			
					<img src="/SysHospital/images/modules/assistencial/Lesoes.png"  />
					<img src="/SysHospital/images/modules/assistencial/LesoesMobile.png"  />
					<img src="/SysHospital/images/modules/assistencial/MedHospital.png"  />
					<img src="/SysHospital/images/modules/assistencial/MedHospitalMobile.png"  />
					<img src="/SysHospital/images/modules/assistencial/Bloco.png"  />
					<img src="/SysHospital/images/modules/assistencial/Phision.png"  />
					<img src="/SysHospital/images/modules/assistencial/Nutri.png"  />
				</div> 
				
				<div> 
					<h3>Gest�o de hotelaria</h3>
					<br><br> 
					<a href="/SysHospital/hotelaria/index"><img src="/SysHospital/images/modules/hotelaria/Recepcao.png"  /></a>
					<img src="/SysHospital/images/modules/hotelaria/Engenharia.png"  />
					<img src="/SysHospital/images/modules/hotelaria/Rouparia.png"  />
					<img src="/SysHospital/images/modules/hotelaria/Higienizacao.png"  />
				</div> 
				
				<div>   
					<h3>Gest�o de Apoio</h3>
					<br><br> 
					<img src="/SysHospital/images/modules/apoio/Banco.png"  />
					<img src="/SysHospital/images/modules/apoio/CCIH.png"  />
					<img src="/SysHospital/images/modules/apoio/CME.png"  />
					<img src="/SysHospital/images/modules/apoio/Pharma.png"  />
					<img src="/SysHospital/images/modules/apoio/Lab.png"  />
					<img src="/SysHospital/images/modules/apoio/Estoque.png"  />
					<img src="/SysHospital/images/modules/apoio/SADT.png"  />
					<img src="/SysHospital/images/modules/apoio/Seguranca.png"  />
				</div> 
				<div>   
					<h3>Gest�o Administrativa e Financeira</h3>
					<br><br> 
					<img src="/SysHospital/images/modules/admFinac/Fiscal.png"  />
					<img src="/SysHospital/images/modules/admFinac/RH.png"  />
					<img src="/SysHospital/images/modules/admFinac/Custos.png"  />
					<img src="/SysHospital/images/modules/admFinac/Faturamento.png"  />
					<img src="/SysHospital/images/modules/admFinac/Financeiro.png"  />
					<img src="/SysHospital/images/modules/admFinac/Contabil.png"  />
				</div> 
				<div>   
					<h3>Gest�o Estrat�gica</h3>
					<br><br> 
					<img src="/SysHospital/images/modules/estrategica/Report.png"  />
					<img src="/SysHospital/images/modules/estrategica/Qualidade.png"  />
					<img src="/SysHospital/images/modules/estrategica/Planejamento.png"  />
				</div> 
				<div>   
					<h3>Administra��o do sistema</h3>
					<p>Somente visivel aos administradores do sistema</p>
					<br><br> 
					<a href="/SysHospital/sistema/index"><img src="/SysHospital/images/modules/administrador.png" /></a>
				</div> 
			 
			</div> 
			<br clear="all" /> 
			<br clear="all" /> 
			<script> 
			$(function() {
				$("#nav ul").tabs("#panes > div", {effect: 'fade', fadeOutSpeed: 400});
			});
			</script> 
			
			    <br /> 
			 
			</div>
			<script type="text/javascript">
			var triggers = $("button.modalInput").overlay({ 
			 
			    // some expose tweaks suitable for modal dialogs 
			    expose: { 
			        color: '#333', 
			        loadSpeed: 200, 
			        opacity: 0.9 
			    }, 
			 
			    closeOnClick: false 
			});
			var buttons = $("#yesno button").click(function(e) { 
			     
			    // get user input 
			    var yes = buttons.index(this) === 0; 
			 
			    // do something with the answer 
			    triggers.eq(0).html("You clicked " + (yes ? "yes" : "no")); 
			});
			$("#prompt form").submit(function(e) { 
			 
			    // close the overlay 
			    triggers.eq(1).overlay().close(); 
			 
			    // get user input 
			    var input = $("input", this).val(); 
			 
			    // do something with the answer 
			    triggers.eq(1).html(input); 
			 
			    // do not submit the form 
			    return e.preventDefault(); 
			});
			</script>
			
			
			<!-- termina submenu -->
			<!-- start content -->	
			<span class="title"><a href="#" style="text-decoration:none;"><n:messages/>&nbsp;</a></span>
			
			<div class="entry">		
				<jsp:include page="${bodyPage}" />							
			</div>
		</div>
		<!-- end page -->
		
		<div id="footer">
			<p class="legal">Copyright (c) 2007 OrionX. All rights reserved.</p>
			<p class="credit">Designed by <a href="http://www.orionx.com.br/">OrionX</a> </p>
		</div>
		
<script type="text/javascript">
//tab
$(function() {

$("#accordion").tabs("#accordion div", {
	tabs: 'img', 
	effect: 'horizontal'
});
});


$(document).ready(function() {
	/*resultados();*/ 
	$("#accordion").hide();
	$("#closeMenuBar").hide();	
	$("#demo img[title]").tooltip({tip: '#demotip', effect: 'bouncy'});
});


// select all desired input fields and attach tooltips to them 
$("#myform :input").tooltip({ 
 
    // place tooltip on the right edge 
    position: "center right", 
 
    // a little tweaking of the position 
    offset: [-2, 10], 
 
    // use the built-in fadeIn/fadeOut effect 
    effect: "fade", 
 
    // custom opacity setting 
    opacity: 0.7, 
 
    // use this single tooltip element 
    tip: '.tooltip' 
 
});

// create custom animation algorithm for jQuery called "bouncy" 
$.easing.bouncy = function (x, t, b, c, d) { 
    var s = 1.70158; 
    if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b; 
    return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b; 
} 
 
// create custom tooltip effect for jQuery Tooltip 
$.tools.tooltip.addEffect("bouncy", 
 
    // opening animation 
    function(done) { 
        this.getTip().animate({top: '+=15'}, 500, 'bouncy', done).show(); 
    }, 
 
    // closing animation 
    function(done) { 
        this.getTip().animate({top: '-=15'}, 500, 'bouncy', function()  { 
            $(this).hide(); 
            done.call(); 
        }); 
    } 
);
var triggers = $("button.modalInput").overlay({ 
 
    // some expose tweaks suitable for modal dialogs 
    expose: { 
        color: '#333', 
        loadSpeed: 200, 
        opacity: 0.9 
    }, 
 
    closeOnClick: false 
});
var buttons = $("#yesno button").click(function(e) { 
     
    // get user input 
    var yes = buttons.index(this) === 0; 
 
    // do something with the answer 
    triggers.eq(0).html("You clicked " + (yes ? "yes" : "no")); 
});
function openMenu(opt){
	if(document.getElementById("accordion").style.display=="none")
		$("#accordion").slideDown();			
	else
		$("#accordion").slideUp();		
}
function applyMask(){
	jQuery(function($){
		$("input[@mask=cep]").mask("99999-999");
		$("input[@mask=cpf]").mask("999.999.999-99");
		$("input[@mask=cnpj]").mask("99.999.999/9999-99");
		$("input[@mask=date]").mask("99/99/9999",{alertNotCompleted:"Formato da data inv�lido."});
		$("input[@mask=mesano]").mask("99/9999");
		$("input[@mask=telefone]").mask("(99) 9999-9999");
	});
	
	 window.onbeforeunload = function(){
			//return "sdasds";
	}
}
</script>
<script language="javascript" type="text/javascript">
function resultados()
{
	var navegador = new BROWSER();
	//var suporte	= new SUPORT();
	var resultado = "Firefox: "+navegador.isFirefox+
					"Internet Explorer:"+navegador.isIE+
					"Mozilla:"+navegador.isMozilla+
					"Opera:"+navegador.isOpera+
					"Chrome:"+navegador.isChrome+
					"Suporte a Design Mode:"+navegador.suport.designMode+
					"Suporte a XMLDOM:"+navegador.suport.XMLDOM+
					"Suporte a XMLHttpRequest:"+navegador.suport.XMLHttpRequest;
					alert(resultado);
	
}
</script>
	</body>
</html>
