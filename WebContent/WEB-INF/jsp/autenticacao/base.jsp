<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<HTML>
	<HEAD>
		<n:head/>
	</HEAD>
	<BODY leftmargin="0" topmargin="0" class="boddy">
		<div class="applicationTitle">
		<a href="${application}">${application}</a>
		</div>
		
		<div class="messageOuterDiv">
			<n:messages/>&nbsp;
		</div>
		<div class="body">
			<jsp:include page="${bodyPage}" />
		</div>
		
	</BODY>
</HTML>