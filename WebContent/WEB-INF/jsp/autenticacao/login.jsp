<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Orionx
http://www.orionx.com.br
-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
	<title>SisHospital</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	</head>
	<body>
	<br><br><br><br>
	<script type="text/javascript" src="js/jquery.tools.min.js"></script>

	<div id="sub-header">	
		
		<br>
	</div>
	
	
	<hr />
	<!-- start page -->
	<div id="page">
		<center>
		<form class="expose"> 
				<div class="tooltip"></div> 
					<n:form validateFunction="validaCamposLogin" >
						<n:bean name="usuario">
							<t:propertyConfig renderAs="double" mode="input" >
								<n:panelGrid columns="2" align="center" >
									<t:property name="login" />
									<t:property name="senha" type="password" label="Senha"/>
									<div align="center">
										<n:submit type="submit" action="doLogin" panelColspan="2" panelAlign="right">Logar</n:submit>
									</div>
								</n:panelGrid>
							</t:propertyConfig>
						</n:bean>
					</n:form>
			</form>	
		</center>
</div>
<!-- end page -->
<div id="footer">
	<p class="legal">Copyright (c) 2007 OrionX. All rights reserved.</p>
	<p class="credit">Designed by <a href="http://www.orionx.com.br/">OrionX</a> </p>
</div>
<script type="text/javascript">
function validaCamposLogin(){
	login = trim(document.getElementsByName('login')[0].value);
	senha = trim(document.getElementsByName('senha')[0].value);
	erros='';
	if(login==null||login==undefined||login.length==0)
		erros=erros+'\n'+'O campo Login � obrigat�rio';
	if(senha==null||senha==undefined||senha.length==0)
		erros=erros+'\n'+'O campo Senha � obrigat�rio';
	if(erros.length!=0){
		alert(erros);
		return false;
	}
	return true;
}	
// execute your scripts when the DOM is ready. this is a good habit 
$(function() { 
 
    // expose the form when it's clicked or cursor is focused 
    $("form.expose").bind("click keydown", function() { 
 
        $(this).expose({ 
 
            // custom mask settings with CSS 
            maskId: 'mask', 
 
            // when exposing is done, change form's background color 
            onLoad: function() { 
                this.getExposed().css({backgroundColor: '#c7f8ff'}); 
            }, 
 
            // when "unexposed", return to original background color 
            onClose: function() { 
                this.getExposed().css({backgroundColor: null}); 
            }, 
 
            api: true 
 
        }).load(); 
    }); 
});
</script>
<style type="text/css">
table {
	background: url(http://static.flowplayer.org/img/global/gradient/h600.png) repeat-x scroll 0 0;
	font-size: 12px;
	border: 0px solid rgb(148, 144, 135); 
	padding: 2px; 
	width: 250px;
}
.applicationTitle{
	display:none;
}
</style>
</body>
</html>

