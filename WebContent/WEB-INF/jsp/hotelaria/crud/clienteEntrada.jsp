<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<span class="titulo">Dados Pessoais</span>
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" style="width:600px;" columnStyles="width:200px,width:900px;">
						<t:property name="nome"/>
						<t:property name="cpf"/>
						<t:property name="rg"/>
						<t:property name="emissaoRg"/>
						<t:property name="orgaoExp"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="idPessoa"/>
						<t:property name="matricula"/>
						<t:property name="email"/>
						<t:property name="telefone"/>
						<t:property name="celular"/>
					</n:panelGrid>
				</n:panelGrid>
				<span class="titulo">Endere�o</span>
				<n:panelGrid columns="2" columnStyles="width:100px,width:100px">
					<n:panelGrid columns="2">
						<t:property name="endereco"/>
						<t:property name="bairro"/>
						<t:property name="observacao"/>
						<t:property name="idPessoa"/>
						<t:property name="email"/>
						<t:property name="nome"/>
					</n:panelGrid>
					<n:panelGrid columns="2">
						<t:property name="observacao"/>
						<t:property name="telefone"/>
					</n:panelGrid>
				</n:panelGrid>


				<span class="titulo">Documentos</span>
				<n:panelGrid columns="2" columnStyles="width:500px,width:300px">
					<n:panelGrid columns="2" width="500px;">
						<t:property name="celular"/>

					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:350px,width:600px">
						<t:property name="cep"/>
						<n:comboReloadGroup useAjax="true">
							<t:property name="uf"/>
							<t:property name="municipio"/>
						</n:comboReloadGroup>
					</n:panelGrid>
				</n:panelGrid>

				<span class="titulo">Dados banc�rios</span>
				<n:panelGrid columns="2" columnStyles="width:100px,width:100px">
					<n:panelGrid columns="2" width="500px;">
						<t:property name="dtNascimento"/>
						<t:property name="naturalidade"/>
					</n:panelGrid>
						
					<n:panelGrid columns="2" colspan="4" columnStyles="width:200px,width:500px;" id="dadosConta">
						
					</n:panelGrid>
				</n:panelGrid>

				<span class="titulo">Acesso</span>
					<n:panelGrid columns="2" propertyRenderAsDouble="true">
						<n:panel colspan="2">
							<table width="100%"><tr>
								<td width="50%">
								</td>
							</tr>
							</table>
						</n:panel>
					</n:panelGrid>
				</n:panel>
			
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
