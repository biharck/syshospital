<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<s:tableGroup columns="2" panelgridWidth="100">
				<t:property name="uf" />
				<t:property name="nome" style="width:250px;"/>
			</s:tableGroup>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="nome" />
			<t:property name="uf.sigla" label="UF"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>