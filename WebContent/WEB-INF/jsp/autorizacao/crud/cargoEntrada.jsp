<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
		<t:property name="idCargo" type="hidden" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<t:property name="nome"/>
			<t:property name="descricao"/>
		</t:tabelaEntrada>
		<t:detalhe name="listaEspecialidade">
			<t:property name="nome"/>
			<t:property name="descricao"/>
			<t:acao>
				<t:propertyConfig renderAs="single">
					<t:property name="idEspecialidade" type="hidden"/>
				</t:propertyConfig>
			</t:acao>
		</t:detalhe>
	</t:janelaEntrada>
</t:entrada>
