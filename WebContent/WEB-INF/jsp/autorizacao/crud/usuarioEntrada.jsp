<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada >
	<t:janelaEntrada >
		<t:tabelaEntrada>
			<t:property name="id"/>
			<t:property name="nome"/>
			<t:property name="login"/>
			<t:property name="senha"/>
			<n:panel valign="top" style="padding-top:4px">Papeis</n:panel>
			<n:dataGrid itemType="br.com.orionx.authorization.bean.Papel" itens="${listaPapel}" bodyStyleClasses="," styleClass=",">
				<n:column width="20">
                    <n:input name="papeis" value="${row}" type="checklist" itens="${usuario.papeis}"/>
                </n:column>
                <n:column>
	                <t:property name="descricao" mode="output"/>
                </n:column>
			</n:dataGrid>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
