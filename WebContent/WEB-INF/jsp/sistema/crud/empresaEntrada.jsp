<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<div class="tooltip"></div>
<t:entrada >	
	<t:property name="idEmpresa" type="hidden" write="false"/>
	<t:janelaEntrada  submitConfirmationScript="valida()">
		<t:tabelaEntrada >
			<t:property name="razaoSocial" style='width:500px;' id="razaosocial"/>
			<t:property name="cnpj" title="CNPJ" id="cnpj" style='width:110px;'/>
			<t:property name="cnes"/>
			<t:property name="endereco" title="Informe o Endere�o como rua, Av. etc..." style='width:500px;' id="endereco"/>
			<t:property name="bairro" style='width:300px;' id="bairro"/>
			<t:property name="cep" id="idcep" style='width:60px;'/>
			<n:comboReloadGroup useAjax="true">
				<t:property name="uf"/>
				<t:property name="municipio"/>
			</n:comboReloadGroup>
			<t:property name="telefoneGeral" id="idtelefone" style='width:80px;'/>
			<t:property name="ramal"/>
			<t:property name="fax" id="idfax" style='width:80px;'/>
			<t:property name="email" style='width:300px;' id="email" />
			<n:panel>Confirma��o de email</n:panel>
				<n:panel>
					<t:property name="confirmaEmail" id="novoemail" onblur="validaEmail()" class="required"/>
					<span id="confirm_alert_email" style='width:300px;' class="warning" > A confirma��o de email n�o confere</span>
			</n:panel>
			<t:property name="incricaoEstadual"/>
			<t:property name="inscricaoMunicipal"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
$(document).ready(function() {
	$("#cnpj").mask("99.999.999/9999-99");
	$("#idcep").mask("99.999-999");
	$("#idtelefone").mask("(99)9999-9999");
	$("#idfax").mask("(99)9999-9999");

});

function validaEmail(){
    boolEmail = $("#email").val() == $("#novoemail").val();	
	alertConfEmail(boolEmail);

}

function alertConfEmail(show){
	if(show){
		$("#confirm_alert_email").fadeOut();
	}else{
		$("#confirm_alert_email").fadeIn();
	}
}
function valida(){
	boolEmail = $("#email").val() == $("#novoemail").val();	
	if(boolEmail){
		alert('A Redigita��o do E-mail N�o Confere.');
		alertConfEmail(boolEmail);
		return false;
	}
	else return true;
}
</script>